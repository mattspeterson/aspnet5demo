using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MyFirstRealWebApp
{
    public class Course
    {
        #region Properties
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }            
        #endregion
        
        public static IEnumerable<Course> GetByName(string name)
        {
            string connectionString = "Data Source=X6436NEW;Initial Catalog=DotNet5Test;MultipleActiveResultSets=False;User Id=DotNet5TestUser;Password=letmein;";
            string sql = "SELECT * FROM Course WHERE Name LIKE @Name";
            
            List<Course> courses = new List<Course>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand(sql, connection))                
                {
                    command.Parameters.AddWithValue("@Name", "%" + name + "%");
                    SqlDataReader reader = command.ExecuteReader();
                    
                    while (reader.Read())
                    {
                        Course course = new Course();
                        
                        course.Id = (Guid)reader["Id"];
                        course.Code = (string)reader["Code"];
                        course.Name = (string)reader["Name"];
                        course.Location = (string)reader["Location"];
                        course.IsActive = (bool)reader["IsActive"];
                        course.CreatedOn = (DateTime)reader["CreatedOn"];
                        
                        courses.Add(course);
                    }
                }
            }
            
            return courses;
        }
    }
}