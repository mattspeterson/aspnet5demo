using System.Collections.Generic;

using Microsoft.AspNet.Mvc;

namespace MyFirstRealWebApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult CourseSearch(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return View();

            IEnumerable<Course> courses = Course.GetByName(name);
            return View(courses);
        }
    }
}
